// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'msg_taxi.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MsgTaxiAdapter extends TypeAdapter<MsgTaxi> {
  @override
  final int typeId = 3;

  @override
  MsgTaxi read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MsgTaxi(
      fields[0] as String,
      fields[1] as String,
      fields[2] as String,
      fields[3] as String,
      fields[4] as String,
    );
  }

  @override
  void write(BinaryWriter writer, MsgTaxi obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.nameUser)
      ..writeByte(1)
      ..write(obj.numberTaxi)
      ..writeByte(2)
      ..write(obj.numberUser)
      ..writeByte(3)
      ..write(obj.latUser)
      ..writeByte(4)
      ..write(obj.longUser);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MsgTaxiAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
