import 'package:get/get.dart';
import 'package:taxi_driving/controller/auth_controller.dart';
import 'package:taxi_driving/controller/get_location.dart';
import 'package:taxi_driving/controller/init_controller.dart';
import 'package:taxi_driving/controller/taxi_location_controller.dart';

class SampleBind extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<GetLocationController>(() => GetLocationController());
    Get.lazyPut<InitController>(() => InitController());
    Get.lazyPut<AuthController>(() => AuthController());
    Get.lazyPut<TaxiLocationController>(() => TaxiLocationController());
  }
}