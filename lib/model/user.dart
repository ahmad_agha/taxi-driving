import 'package:hive/hive.dart';
import 'package:taxi_driving/core/constant.dart';

part 'user.g.dart';

@HiveType(typeId: user)
class User extends HiveObject {

  @HiveField(0)
  final String name;

  @HiveField(1)
  final String number;

  @HiveField(2)
  final String password;

  User(this.name,  this.number,  this.password,);
}