import 'package:hive/hive.dart';
import 'package:taxi_driving/core/constant.dart';

part 'init_location.g.dart';

@HiveType(typeId: myLocation)
class InitLocation extends HiveObject {

  @HiveField(0)
  final String lat;

  @HiveField(1)
  final String lang;

  @HiveField(2)
  final String taxiId;

  InitLocation(this.lat,  this.lang, this.taxiId);
}