import 'package:flutter/material.dart';

class MyTextButton extends StatelessWidget {
  final Color? color;
  final String title;
  final VoidCallback onPressed;
  final double fontSize;
  final FontWeight fontWeight;

  const MyTextButton(
      {Key? key,
      this.color,
      required this.title,
      required this.onPressed,
      this.fontSize = 18,
      this.fontWeight = FontWeight.bold})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: Text(
          title,
          style: TextStyle(
              color: color?? Colors.white
          ),
      ),
      style: ButtonStyle(
        textStyle: MaterialStateProperty.all<TextStyle>(
            TextStyle(fontWeight: fontWeight, fontSize: fontSize)),
        padding: MaterialStateProperty.all<EdgeInsets>(
           EdgeInsets.zero)
      ),
    );
  }
}
