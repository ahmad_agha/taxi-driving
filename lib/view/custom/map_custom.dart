// ignore_for_file: avoid_print

import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:taxi_driving/controller/get_location.dart';
import 'package:taxi_driving/controller/taxi_location_controller.dart';
import 'package:taxi_driving/core/constant.dart';
import 'package:taxi_driving/core/widget/toast.dart';

class MapCustom extends StatefulWidget {
  final Uint8List customIcon;
  final GetLocationController getxController;

  const MapCustom(
    this.getxController, {
    Key? key,
    required this.customIcon,
  }) : super(key: key);

  @override
  _MapCustomState createState() => _MapCustomState();
}

class _MapCustomState extends State<MapCustom> {
  // static final LatLng center =
  //     LatLng(double.parse(UserInfo.lat!), double.parse(UserInfo.long!));
  List<Marker> customMarkers = [];

  late GoogleMapController googleMapController;

  final controller = Get.put(TaxiLocationController());

  void _onMapCreated(GoogleMapController controller) {
    googleMapController = controller;
  }

  void _createMarker() {
    int i = 0;

    while (i <= widget.getxController.taxisLocation.length + 1) {
      print('-----while----');
      if (customMarkers.isEmpty) {
        customMarkers.add(Marker(
          markerId: const MarkerId("marker_1"),
          position: LatLng(double.parse(widget.getxController.lat),
              double.parse(widget.getxController.long)),
          draggable: true,
          flat: true,
          onDragEnd: ((newPosition) {
            widget.getxController.setLocation(
                newPosition.latitude.toString(), newPosition.longitude.toString());
            print(
                '================${newPosition.latitude} ${newPosition.longitude}=============');
          }),
        ));
      } else {
        print('----------taxi location---------------');
        for (Map<String, String> e in widget.getxController.taxisLocation) {
          print('----------e $e---------------');
          customMarkers.add(Marker(
              markerId: MarkerId(e['taxiId']!),
              position:
                  LatLng(double.parse(e['lat']!), double.parse(e['lang']!)),
              icon: BitmapDescriptor.fromBytes(widget.customIcon),
              draggable: false,
              flat: true));
        }
      }
      i++;
    }
  }

  @override
  void initState() {
    super.initState();
    _createMarker();
  }

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: const Text('اختر موقعك و اطلب تكسي'),
        centerTitle: true,
        backgroundColor: Colors.deepOrange,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async{
          bool added= await widget.getxController.getTaxi();
          if (added) {
            toastMsg('تم الطلب بنجاح', isError: false);
            Timer(const Duration(seconds: 1),
                    () => Navigator.of(context).pop());
          }
          // controller.getTaxi();
        },
        child: const Icon(Icons.local_taxi_outlined),
      ),
      body: Stack(
        children: [
          SizedBox(
            height: _size.height,
            width: _size.width,
            child: GoogleMap(
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                target: LatLng(
                    double.parse(UserInfo.lat!), double.parse(UserInfo.long!)),
                zoom: 14.0,
              ),
              markers: customMarkers.toSet(),
              myLocationEnabled: true,
              myLocationButtonEnabled: true,
              mapType: MapType.terrain,
              compassEnabled: true,
              tiltGesturesEnabled: false,
            ),
          ),
        ],
      ),
    );
  }
}
