import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void toastMsg(String errorCode,{bool isError=true}) {
  if (errorCode.isNotEmpty) {
    Fluttertoast.showToast(
        msg: errorCode,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 2,
        //timeInSecForIos: 1,
        backgroundColor: isError? Colors.red.withOpacity(0.8): Colors.green.withOpacity(0.8),
        textColor: Colors.white,
        fontSize: 16.0);
  }
}