// ignore_for_file: avoid_print

import 'dart:math';

import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:location/location.dart';
import 'package:taxi_driving/core/constant.dart';
import 'package:taxi_driving/model/init_location.dart';
import 'package:taxi_driving/model/msg_taxi.dart';

class GetLocationController extends GetxController {
  late String lat;
  late String long;
  var getAccess = false.obs;
  late Box locationBox = Hive.box<InitLocation>(myLocationBox);
  late Box msgBox = Hive.box<MsgTaxi>(msgTaxiBox);
  final Location location = Location();
  List<Map<String, String>> taxisLocation = [];
  List<Map<String, dynamic>> totalDistance = [];
  late double minDistance;
  late String taxiId;

  Future<dynamic> getServiceEnabledAndPermissionStatus() async {
    bool _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    PermissionStatus _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        print('---------------------------------');
        print(_permissionGranted);
        return;
      }
    }
    getAccess = true.obs;
    await getLocation();
  }

  Future<void> getLocation() async {
    LocationData myLoc = await location.getLocation();
    lat = myLoc.latitude.toString();
    long = myLoc.longitude.toString();
    update();
    UserInfo.lat = lat;
    UserInfo.long = long;

    print("-------------lat $lat------------");
    print("-------------lang $long------------");
  }

  void setLocation(String lat, String long) {
    print('------------');
    this.lat = lat;
    this.long = long;
    print('+++++++++${this.lat}, ${this.long}++++++++++++');
    update();
  }

  Future<bool> addLocation() async {
    final Map<dynamic, dynamic> location = locationBox.toMap();
    dynamic locationKey;
    final addLocation = InitLocation(lat, long, UserInfo.userId!);

    print(UserInfo.userId);

    location.forEach((key, value) {
      if (value.taxiId == UserInfo.userId) {
        locationKey = key;
        print('-------------$locationKey--------------');
      }
    });

    if (locationKey == null) {
      await locationBox.add(addLocation);
      return true;
    } else {
      await locationBox.put(locationKey, addLocation);
      return true;
    }
  }

  void getAllTaxisLocation() {
    taxisLocation.clear();
    final Map<dynamic, dynamic> location = locationBox.toMap();
    location.forEach((key, value) {
      taxisLocation
          .add({'lat': value.lat, 'lang': value.lang, 'taxiId': value.taxiId});
    });
    // update();
  }

  double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  Future<bool> getTaxi() async {
    totalDistance = [];
    print(locationBox.values);
    for(Map<String, String> e in taxisLocation){
      totalDistance.add({
        "id": e['taxiId'],
        "distance": calculateDistance(
            double.parse(lat),
            double.parse(long),
            double.parse(e["lat"]!),
            double.parse(e["lang"]!))
      });
    }
    print(totalDistance);
    // print(totalDistance.reduce(max(a, b)));
        if (totalDistance.length==1) {
          minDistance = await totalDistance[0]['distance'];
          taxiId = await totalDistance[0]['id'];
        }  else {
      for (int i = 0; i < totalDistance.length - 1; i++) {
        if (totalDistance[i]['distance'] < totalDistance[i + 1]['distance']) {
          minDistance = await totalDistance[i]['distance'];
          taxiId = await totalDistance[i]['id'];
        } else {
          minDistance = await totalDistance[i + 1]['distance'];
          taxiId = await totalDistance[i + 1]['id'];
        }
      }
    }
    // print(minDistance);
    // print(taxiId);

    if (taxiId.isNotEmpty) {
      await sendMsg(taxiId);
      // update();
      return true;
    }  else{
      return false;
    }

  }

  Future<void> sendMsg(String taxiId)async{
    final addMsg = MsgTaxi(UserInfo.userName!, taxiId, UserInfo.userId!, lat,long);
    msgBox.add(addMsg);
    update();
  }


}
