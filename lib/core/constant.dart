import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/services.dart';

const int user = 0;
const int taxiDriver = 1;
const int myLocation = 2;
const int msgTaxi = 3;
const String userBox ='user_box';
const String taxiDriverBox ='taxi_driver_box';
const String myLocationBox ='my_location_box';
const String msgTaxiBox ='Msg_Taxi_box';
const String userId = "user_Id";
const String userName = "user_name";
const String isLogin = "is_login";
const String isTaxi = "is_taxi";

class UserInfo {
  static String? lat;
  static String? long;
  static String? userId;
  static String? userName;
  static bool? isLogin;
  static bool? isTaxi;
}




Future<Uint8List> getBytesFromAsset(String path, int width) async {
  ByteData data = await rootBundle.load(path);
  ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
  ui.FrameInfo fi = await codec.getNextFrame();
  return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!.buffer.asUint8List();
}