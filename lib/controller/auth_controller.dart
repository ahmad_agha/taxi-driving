// ignore_for_file: avoid_print

import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxi_driving/controller/sample_bind.dart';
import 'package:taxi_driving/core/constant.dart';
import 'package:taxi_driving/model/taxi_driver.dart';
import 'package:taxi_driving/model/user.dart';
import 'package:taxi_driving/view/splash_screen.dart';

class AuthController extends GetxController {
  late Box userResponseBox = Hive.box<User>(userBox);
  late Box taxiDriverResponseBox = Hive.box<TaxiDriver>(taxiDriverBox);
  List<String> numberList = [];
  RxBool isLoading = false.obs;
  RxBool showFirst = true.obs;
  RxBool useNumber = false.obs;
  RxBool isValidateLogin = false.obs;

  void getAllNumbers() {
    isLoading = true.obs;
    numberList = [];
    for (var e in userResponseBox.values) {
      numberList.add(e.number);
      print('---------user: ${e.number}--------');
    }
    for (var e in taxiDriverResponseBox.values) {
      numberList.add(e.number);
      print('---------taxiDriverResponseBox: ${e.number}--------');
    }
    print(numberList);
    isLoading = false.obs;
    update();
  }

  void changeShowFirst() {
    showFirst = (!showFirst.value).obs;
    print(showFirst);
    update();
  }

  void validatorNumber({required String number}) {
    useNumber = false.obs;
    for (var e in numberList) {
      if (e == number) {
        useNumber = true.obs;
      }
    }
    update();
  }

  Future<void> signUp(
      {required String name,
      required String password,
      required String number,
      String? officeName}) async {
    SharedPreferences prefer = await getPreferences();
    isLoading = true.obs;
    update();
    try {
      if (officeName == null) {
        final addUser = User(
          name,
          number,
          password,
        );
        var adds = await userResponseBox.add(addUser);
        print("===============$adds==================");

        print("===============${addUser.number}==================");


        UserInfo.isLogin = true;
        await   prefer.setBool(isLogin,true);
        UserInfo.userId = number;
        await prefer.setString(userId,number);
        UserInfo.userName = name;
        await  prefer.setString(userName,name);
        UserInfo.isTaxi = false;
        await   prefer.setBool(isTaxi,false);

      } else {

        final addTaxi = TaxiDriver(
          name,
          number,
          password,
          officeName
        );

       await taxiDriverResponseBox.add(addTaxi);
        UserInfo.isLogin = true;
        await   prefer.setBool(isLogin,true);
        UserInfo.userId = number;
        await prefer.setString(userId,number);
        UserInfo.userName = name;
        await  prefer.setString(userName,name);
        UserInfo.isTaxi = true;
        await   prefer.setBool(isTaxi,true);
      }
      isLoading = false.obs;
      update();
    } catch (e) {
      isLoading = false.obs;
      update();
      rethrow;
    }
  }

  Future<void> validatorPasswordLogin(
      {required String password,
      required String number,
      required bool isOffice}) async{

    SharedPreferences prefer = await getPreferences();
    isLoading = true.obs;
    update();
    print('-------------------validatorPasswordLogin---------------------');
    if (isOffice) {
      print('--------------$isOffice-------------');
      for (var e in taxiDriverResponseBox.values) {
        print('--------------e ${e.number}-------------');
        if (e.number == number) {
          print('--------------$number-------------');
          if (e.password == password) {
            print('--------------$password-------------');
            UserInfo.isLogin = true;
            await   prefer.setBool(isLogin,true);
            UserInfo.userId = number;
            await prefer.setString(userId,number);
            UserInfo.userName = e.name;
            await  prefer.setString(userName,e.name);
            UserInfo.isTaxi = isOffice;
            await   prefer.setBool(isTaxi,isOffice);
            print('+++++++++++++${UserInfo.isLogin}+++++++++++++++++++');
            break;
          }else{
            isValidateLogin=true.obs;
            update();
          }
        }
      }
    } else if (!isOffice) {
      for (var e in userResponseBox.values) {
        if (e.number == number) {
          if (e.password == password) {
            UserInfo.isLogin = true;
           await prefer.setBool(isLogin,true);
            UserInfo.userId = number;
            await prefer.setString(userId,number);
            UserInfo.userName = e.name;
            await  prefer.setString(userName,e.name);
            UserInfo.isTaxi = isOffice;
            await prefer.setBool(isTaxi,isOffice);
            print('+++++++++++++${UserInfo.isLogin}+++++++++++++++++++');
            break;
            // isValidateLogin=true.obs;
            // update();
          }
        }else{
          isValidateLogin=true.obs;
          update();
        }
      }
    }
    print('+++++++++++++${UserInfo.isLogin}+++++++++++++++++++');
    isLoading = false.obs;
    update();
  }

  Future<SharedPreferences> getPreferences() async =>
      await SharedPreferences.getInstance();


  void clearIsValidateLogin(){
    isValidateLogin=false.obs;
    update();
  }


  Future<void> logout()async{

    SharedPreferences prefer = await getPreferences();

    UserInfo.isLogin = false;
    await   prefer.setBool(isLogin,false);
    UserInfo.userId = '';
    await  prefer.setString(userId,'');
    UserInfo.userName = '';
    await  prefer.setString(userName,'');
    UserInfo.isTaxi = false;
    await   prefer.setBool(isTaxi,false);
    Get.offAll(const SplashScreen(),binding: SampleBind());
    // Get.clearRouteTree();
    // Get.to( () => const SplashScreen(), binding: SampleBind());
  }


}
