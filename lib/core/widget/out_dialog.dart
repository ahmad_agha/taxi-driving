import 'dart:ui';

import 'package:flutter/material.dart';

Future<bool> outDialog(
    BuildContext context,
    ) async {
  bool item = await showDialog(
    context: context,
    barrierDismissible: true,
    builder: (context) {
      return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
        child: AlertDialog(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
          title: const Text('إغلاق التطبيق'),
          content: const Text('هل ترغب بالإغلاق التطبيق'),
          actions: [
            TextButton(
              child: const Text(
                "لا",
                style: TextStyle(color: Colors.red),
              ),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            TextButton(
              child: const Text(
                "نعم",
                style: TextStyle(color: Colors.green),
              ),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            )
          ],
        ),
      );
    },
  );
  return item;
}