import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String title;
  final double? height;
  final double? width;
  final Color? color;
  final double? elevation;
  final Color? textColor;
  final double? fontSize;
  final FontWeight? fontWeight;
  final double? borderRadius;
  final EdgeInsetsGeometry? padding;

  const MyButton(
      {Key? key,
      required this.onPressed,
      required this.title,
      this.height,
      this.width,
      this.color,
      this.elevation = 0,
      this.textColor=Colors.white,
      this.fontSize=16,
      this.fontWeight,
      this.borderRadius=12,
      this.padding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ButtonStyle(
        elevation: MaterialStateProperty.all<double>(elevation!),
        backgroundColor: MaterialStateProperty.all<Color>(
            color ?? Theme.of(context).colorScheme.primary),
        shape: MaterialStateProperty.all<OutlinedBorder>(RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius!))),
      ),
      child: Container(
        width: width,
        height: height,
        padding: padding,
        child: Center(
            child: Text(
          title,
          style: TextStyle(
              fontWeight: fontWeight, color: textColor, fontSize: fontSize),
        )),
      ),
    );
  }
}
