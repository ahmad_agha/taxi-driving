import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taxi_driving/controller/auth_controller.dart';
import 'package:taxi_driving/controller/get_location.dart';
import 'package:taxi_driving/controller/msg_controller.dart';
import 'package:taxi_driving/controller/sample_bind.dart';
import 'package:taxi_driving/core/constant.dart';
import 'package:taxi_driving/core/widget/badge.dart';
import 'package:taxi_driving/core/widget/my_button.dart';
import 'package:taxi_driving/core/widget/out_dialog.dart';
import 'package:taxi_driving/view/taxi_office/mail_screen.dart';
import 'package:taxi_driving/view/taxi_office/map_taxi.dart';

class HomeTaxiScreen extends StatefulWidget {
  const HomeTaxiScreen({Key? key}) : super(key: key);

  @override
  _HomeTaxiScreenState createState() => _HomeTaxiScreenState();
}

class _HomeTaxiScreenState extends State<HomeTaxiScreen> {
  final authController = Get.put(AuthController());
  final msgController = Get.put(MsgController());
  final locationController = Get.put(GetLocationController());

  @override
  void initState() {
    super.initState();
    msgController.getAllMsg();
    locationController.getServiceEnabledAndPermissionStatus();
  }

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    late Uint8List customIcon;
    return WillPopScope(
      onWillPop: () => _onWillPop(context),
      child: GetBuilder(
        builder: (MsgController controller) {
          return Scaffold(
            appBar: AppBar(
              title: Text(UserInfo.userName ?? ''),
              centerTitle: true,
              backgroundColor: Colors.deepOrange,
              leading:  msgController.msgTaxi.isEmpty? IconButton(
                icon: const Icon(Icons.mail_outline),
                onPressed: () {

                },
              )
              :Badge(
                value: msgController.msgTaxi.length.toString(),
                color: Colors.green[500],
                child: IconButton(
                  icon: const Icon(Icons.mail_outline),
                  onPressed: () {
                    Get.to(()=> MailScreen(msgController), binding: SampleBind());
                  },
                ),
              ),
            ),
            body: Container(
              height: _size.height,
              width: _size.width,
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: _size.height * 0.1),

                  Image.asset(
                    'assets/car-pin.png',
                    height: _size.height * 0.3,
                  ),
                  const SizedBox(height: 30),
                  MyButton(
                      height: 40,
                      onPressed: () async {
                        customIcon= await getBytesFromAsset('assets/car-pin.png',105);
                        Get.to(
                            () => MapTaxi(
                                  customIcon: customIcon,
                                ),
                            binding: SampleBind());
                      },
                      title: 'تحديد/تغير موقع المكتب'),
                  const SizedBox(height: 5),
                  MyButton(
                    height: 40,
                    onPressed: () async {
                      await authController.logout();
                    },
                    title: 'تسجيل الخروج',
                    color: Colors.red[800],
                  ),
                ],
              ),
            ),
          );
        }
      ),
    );
  }

  Future<bool> _onWillPop(
    BuildContext context,
  ) async {
    bool isClosed = await outDialog(
      context,
    );
    if (isClosed) {
      exit(0);
    }
    return false;
  }
}
