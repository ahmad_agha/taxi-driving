import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AuthField extends StatelessWidget {
  final String labelText;
  final String hintText;
  final TextEditingController controller;
  final String iconPath;
  final FormFieldValidator<String?> validator;
  final TextInputType? keyboardType;
  final bool isPassword;
  final Color textColor;

  const AuthField(
      {Key? key,
      required this.labelText,
      required this.controller,
      required this.iconPath,
      required this.validator,
      required this.hintText,
      this.keyboardType,
      this.isPassword=false,
        this.textColor= Colors.black, })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      maxLines: 1,
      validator: validator,
      obscureText: isPassword,
      keyboardType: keyboardType,
      decoration: InputDecoration(
        labelText: labelText,
        alignLabelWithHint: false,
        labelStyle: TextStyle(
          fontSize: 14,
          color: textColor,
        ),
        prefixIcon: Row(
          children: [
            SvgPicture.asset(iconPath,color: Theme.of(context).colorScheme.secondary,),
            const SizedBox(width: 3,),
          ],
        ),
        prefixIconConstraints: const BoxConstraints(maxWidth: 32, maxHeight: 20),
        contentPadding: const EdgeInsets.all(0),
        //labelText: labelText,
        hintText: hintText,
        hintStyle: TextStyle(
          color: textColor.withOpacity(0.2),
        ),
      ),
    );
  }
}
