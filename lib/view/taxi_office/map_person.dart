import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapPerson extends StatefulWidget {

  final String lat;
  final String long;

  const MapPerson({Key? key, required this.lat, required this.long}) : super(key: key);

  @override
  _MapPersonState createState() => _MapPersonState();
}

class _MapPersonState extends State<MapPerson> {


  List<Marker> customMarkers = [];

  late GoogleMapController googleMapController;

  void _onMapCreated(GoogleMapController controller) {
    googleMapController = controller;
  }

  Marker _createMarker() {
    return Marker(
      markerId: const MarkerId("marker_1"),
      position:
      LatLng(double.parse(widget.lat), double.parse(widget.long)),
      draggable: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          title: const Text('موقع الزبون'),
          centerTitle: true,
          backgroundColor: Colors.deepOrange,
        ),
      body: SizedBox(
        height: _size.height,
        width: _size.width,
        child: GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: LatLng(double.parse(widget.lat),
                double.parse(widget.long)),
            zoom: 14.0,
          ),
          markers: <Marker>{_createMarker()},
          myLocationEnabled: true,
          myLocationButtonEnabled: true,
          mapType: MapType.terrain,
          compassEnabled: true,
          tiltGesturesEnabled: false,
        ),
      ),
    );
  }
}
