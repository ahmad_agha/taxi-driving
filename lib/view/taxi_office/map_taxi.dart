// ignore_for_file: avoid_print

import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:taxi_driving/controller/get_location.dart';
import 'package:taxi_driving/core/constant.dart';
import 'package:taxi_driving/core/widget/toast.dart';

class MapTaxi extends StatefulWidget {
  final Uint8List customIcon;

  const MapTaxi({
    Key? key,
    required this.customIcon,
  }) : super(key: key);

  @override
  _MapTaxiState createState() => _MapTaxiState();
}

class _MapTaxiState extends State<MapTaxi> {
  // static final LatLng center =
  // LatLng(double.parse(UserInfo.lat!), double.parse(UserInfo.long!));
  List<Marker> customMarkers = [];

  late GoogleMapController googleMapController;

  final controller = Get.put(GetLocationController());

  void _onMapCreated(GoogleMapController controller) {
    googleMapController = controller;
  }

  Marker _createMarker() {
    return Marker(
      markerId: const MarkerId("marker_1"),
      position:
          LatLng(double.parse(controller.lat), double.parse(controller.long)),
      draggable: true,
      icon: BitmapDescriptor.fromBytes(widget.customIcon),
      onDragEnd: ((newPosition) {
        controller.setLocation(
            newPosition.latitude.toString(), newPosition.longitude.toString());
        print(
            '================${newPosition.latitude} ${newPosition.longitude}=============');
      }),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;

    return GetBuilder(builder: (GetLocationController controllerLoc) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('اختر موقعك'),
          centerTitle: true,
          backgroundColor: Colors.deepOrange,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            bool added = await controller.addLocation();
            if (added) {
              toastMsg('تم إضافة موقعك بنجاح', isError: false);
              Timer(const Duration(seconds: 1),
                  () => Navigator.of(context).pop());
            }
          },
          child: const Icon(Icons.add_location_alt),
        ),
        body: SizedBox(
          height: _size.height,
          width: _size.width,
          child: GoogleMap(
            onMapCreated: _onMapCreated,
            initialCameraPosition: CameraPosition(
              target: LatLng(double.parse(UserInfo.lat!),
                  double.parse(UserInfo.long!)),
              zoom: 14.0,
            ),
            markers: <Marker>{_createMarker()},
            myLocationEnabled: true,
            myLocationButtonEnabled: true,
            mapType: MapType.terrain,
            compassEnabled: true,
            tiltGesturesEnabled: false,
          ),
        ),
      );
    });
  }
}
