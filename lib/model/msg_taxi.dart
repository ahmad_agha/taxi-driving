import 'package:hive/hive.dart';
import 'package:taxi_driving/core/constant.dart';

part 'msg_taxi.g.dart';

@HiveType(typeId: msgTaxi)
class MsgTaxi extends HiveObject {
  @HiveField(0)
  final String nameUser;

  @HiveField(1)
  final String numberTaxi;

  @HiveField(2)
  final String numberUser;

  @HiveField(3)
  final String latUser;

  @HiveField(4)
  final String longUser;


  MsgTaxi(this.nameUser, this.numberTaxi, this.numberUser, this.latUser,
      this.longUser);
}
