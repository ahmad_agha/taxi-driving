import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taxi_driving/controller/sample_bind.dart';
import 'package:taxi_driving/core/widget/my_button.dart';
import 'package:taxi_driving/core/widget/out_dialog.dart';
import 'package:taxi_driving/view/auth_screen.dart';

class FirstScreen extends StatefulWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  _FirstScreenState createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () => _onWillPop(context),
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            width: _size.width,
            height: _size.height,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/taxi-back.jpg'), fit: BoxFit.fill)),
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'الرجاء الاخيار طريقة التسجيل',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: _size.height * 0.04),
                MyButton(
                  onPressed: () {
                    Get.to(
                        () => const TaxiAuthScreen(
                              isOffice: false,
                            ),
                        binding: SampleBind());
                  },
                  title: "التسجيل كزبون",
                  height: 50,
                  width: _size.width * 0.8,
                ),
                SizedBox(height: _size.height * 0.04),
                MyButton(
                  onPressed: () {
                    Get.to(() => const TaxiAuthScreen(
                          isOffice: true,
                        ) ,binding: SampleBind() );
                  },
                  title: "التسجيل كمكتب تكاسي",
                  height: 50,
                  width: _size.width * 0.8,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> _onWillPop(BuildContext context,) async {
    bool isClosed= await outDialog(context,);
    if (isClosed) {
      exit(0);
    }
    return false;
  }



}
