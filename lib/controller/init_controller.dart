// ignore_for_file: avoid_print

import 'dart:io';

import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxi_driving/core/constant.dart';
import 'package:taxi_driving/model/init_location.dart';
import 'package:taxi_driving/model/msg_taxi.dart';
import 'package:taxi_driving/model/taxi_driver.dart';
import 'package:taxi_driving/model/user.dart';

class InitController extends GetxController{



  Future<SharedPreferences> getPreferences() async =>
      await SharedPreferences.getInstance();

  Future<void> getInitData() async {
    SharedPreferences prefer = await getPreferences();
    UserInfo.isLogin = prefer.getBool(isLogin)??false;
    UserInfo.userName = prefer.getString(userName)??'';
    UserInfo.isTaxi = prefer.getBool(isTaxi)??false;
    UserInfo.userId = prefer.getString(userId)??'';
    print(UserInfo.isLogin);
    print(UserInfo.userId);
    print(UserInfo.userName);
    print(UserInfo.isTaxi);
  }

  Future<void> initState() async{
    final Directory appDocumentDirectory =
        await getApplicationDocumentsDirectory();
    print(appDocumentDirectory.path);
    Hive.init(appDocumentDirectory.path);
    await getInitData();
    Hive.registerAdapter<InitLocation>(InitLocationAdapter(),);
    await Hive.openBox<InitLocation>(myLocationBox);
    Hive.registerAdapter<User>(UserAdapter(),);
    await Hive.openBox<User>(userBox);
    Hive.registerAdapter<TaxiDriver>(TaxiDriverAdapter(),);
    await Hive.openBox<TaxiDriver>(taxiDriverBox);
    Hive.registerAdapter<MsgTaxi>(MsgTaxiAdapter(),);
    await Hive.openBox<MsgTaxi>(msgTaxiBox);
    print("-----------------end init----------------------");
  }

}