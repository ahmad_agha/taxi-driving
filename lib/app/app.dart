import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:taxi_driving/controller/get_location.dart';
import 'package:taxi_driving/controller/init_controller.dart';
import 'package:taxi_driving/controller/sample_bind.dart';
import 'package:taxi_driving/view/splash_screen.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {

  final controller = Get.put(GetLocationController());
  final initController = Get.put(InitController());


  @override
  void initState() {
    super.initState();
    initController.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = ThemeData();


        return GetMaterialApp(
            debugShowCheckedModeBanner: false,
            locale: const Locale("ar"),
            localizationsDelegates: const [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: const [
               Locale('en'),
               Locale('ar'),
            ],
            theme: theme.copyWith(
              colorScheme: theme.colorScheme
                  .copyWith(secondary: Colors.amberAccent, primary: Colors.amber[400]),
            ),
          initialRoute: '/splash',
          getPages: [
            GetPage(name: '/splash', page: () => const SplashScreen(), binding: SampleBind()),
          ],
        );

  }
}
