import 'package:flutter/material.dart';
import 'package:taxi_driving/app/app.dart';



void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const App());
}
