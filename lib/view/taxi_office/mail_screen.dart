import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taxi_driving/controller/msg_controller.dart';
import 'package:taxi_driving/core/widget/my_text_button.dart';
import 'package:taxi_driving/view/taxi_office/map_person.dart';

class MailScreen extends StatefulWidget {
  final MsgController msgController;

  const MailScreen(this.msgController, {Key? key}) : super(key: key);

  @override
  _MailScreenState createState() => _MailScreenState();
}

class _MailScreenState extends State<MailScreen> {
  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;

    return GetBuilder(
      builder: (MsgController controller) {
        return Scaffold(
          appBar: AppBar(
            title: const Text('الطلبات'),
            centerTitle: true,
            backgroundColor: Colors.deepOrange,
          ),
          body: Stack(
            children: [
              ListView.builder(
                itemCount: widget.msgController.msgTaxi.length,
                itemBuilder: (context, index) {
                  var msg = widget.msgController.msgTaxi[index];
                  return Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: SizedBox(
                      height: _size.height * 0.15,
                      child: Card(
                        color: Colors.white70,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          // mainAxisSize: MainAxisSize.min,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  "اسم الزبون: " "${msg['nameUser']!}",
                                  style: const TextStyle(
                                      color: Colors.black45,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Text(
                                      "رقم الزبون: ",
                                      style: TextStyle(
                                          color: Colors.black45,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      msg['numberUser']!,
                                      style: const TextStyle(
                                          color: Colors.black45,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                MyTextButton(
                                    title: "مشاهدة موقع الزبون",
                                    color: Colors.deepOrangeAccent,
                                    fontSize: 14,
                                    onPressed: () {
                                      Get.to(() => MapPerson(
                                          lat: msg['latUser']!,
                                          long: msg['longUser']!));
                                    }),
                                MyTextButton(
                                    title: "تمت القراءة",
                                    color: Colors.blueAccent[700],
                                    fontSize: 16,
                                    onPressed: () async{
                                      await widget.msgController.makeRead(msg['key']!);
                                    }),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
              widget.msgController.isLoading.isTrue
                  ? SizedBox(
                child: const Center(child: CircularProgressIndicator()),
                height: _size.height,
                width: _size.width,
              )
                  : Container()
            ],
          ),
        );
      }
    );
  }
}
