// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'taxi_driver.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TaxiDriverAdapter extends TypeAdapter<TaxiDriver> {
  @override
  final int typeId = 1;

  @override
  TaxiDriver read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TaxiDriver(
      fields[0] as String,
      fields[1] as String,
      fields[2] as String,
      fields[3] as String,
    );
  }

  @override
  void write(BinaryWriter writer, TaxiDriver obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.number)
      ..writeByte(2)
      ..write(obj.password)
      ..writeByte(3)
      ..write(obj.officeName);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TaxiDriverAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
