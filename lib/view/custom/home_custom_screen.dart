import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taxi_driving/controller/auth_controller.dart';
import 'package:taxi_driving/controller/get_location.dart';
import 'package:taxi_driving/controller/sample_bind.dart';
import 'package:taxi_driving/core/constant.dart';
import 'package:taxi_driving/core/widget/my_button.dart';
import 'package:taxi_driving/core/widget/out_dialog.dart';
import 'package:taxi_driving/view/custom/map_custom.dart';

class HomeCustom extends StatefulWidget {
  const HomeCustom({Key? key}) : super(key: key);

  @override
  _HomeCustomState createState() => _HomeCustomState();
}

class _HomeCustomState extends State<HomeCustom> {
  final authController = Get.put(AuthController());
  final locationController = Get.put(GetLocationController());
  late Uint8List customIcon;

  @override
  void initState() {
    super.initState();
    locationController.getAllTaxisLocation();
    locationController.getServiceEnabledAndPermissionStatus();
  }

  @override
  Widget build(BuildContext context) {

    final _size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () => _onWillPop(context),
      child: Scaffold(
        appBar: AppBar(
          title: Text(UserInfo.userName ?? ''),
          centerTitle: true,
          backgroundColor: Colors.deepOrange,
          leading: const Icon(Icons.person),
        ),
        body: GetBuilder(builder: (GetLocationController controller) {
          return Container(
            height: _size.height,
            width: _size.width,
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: _size.height * 0.1),
                Image.asset('assets/taxi.png', height: _size.height * 0.3),
                const SizedBox(height: 30),
                MyButton(
                    height: 40,
                    onPressed: () async {
                      customIcon =
                          await getBytesFromAsset('assets/car-pin.png', 105);
                      Get.to(
                          () => MapCustom(
                                locationController,
                                customIcon: customIcon,
                              ),
                          binding: SampleBind());
                    },
                    title: 'تحديد موقعي و طلب تكسي'),
                const SizedBox(height: 5),
                MyButton(
                  height: 40,
                  onPressed: () async {
                    await authController.logout();
                  },
                  title: 'تسجيل الخروج',
                  color: Colors.red[800],
                ),
              ],
            ),
          );
        }),
      ),
    );
  }

  Future<bool> _onWillPop(
    BuildContext context,
  ) async {
    bool isClosed = await outDialog(
      context,
    );
    if (isClosed) {
      exit(0);
    }
    return false;
  }
}
