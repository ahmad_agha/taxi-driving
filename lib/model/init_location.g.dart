// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'init_location.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class InitLocationAdapter extends TypeAdapter<InitLocation> {
  @override
  final int typeId = 2;

  @override
  InitLocation read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return InitLocation(
      fields[0] as String,
      fields[1] as String,
      fields[2] as String,
    );
  }

  @override
  void write(BinaryWriter writer, InitLocation obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.lat)
      ..writeByte(1)
      ..write(obj.lang)
      ..writeByte(2)
      ..write(obj.taxiId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is InitLocationAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
