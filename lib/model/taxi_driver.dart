import 'package:hive/hive.dart';
import 'package:taxi_driving/core/constant.dart';

part 'taxi_driver.g.dart';

@HiveType(typeId: taxiDriver)
class TaxiDriver extends HiveObject {

  @HiveField(0)
  final String name;

  @HiveField(1)
  final String number;

  @HiveField(2)
  final String password;

  @HiveField(3)
  final String officeName;

  TaxiDriver(this.name,  this.number,  this.password, this.officeName);
}