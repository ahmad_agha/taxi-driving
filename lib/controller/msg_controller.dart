// ignore_for_file: avoid_print

import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:taxi_driving/core/constant.dart';
import 'package:taxi_driving/model/msg_taxi.dart';

class MsgController extends GetxController {
  late Box msgBox = Hive.box<MsgTaxi>(msgTaxiBox);
  List<Map<String, String>> msgTaxi = [];
  RxBool isLoading=false.obs;

  void getAllMsg() {
    msgTaxi.clear();
    final Map<dynamic, dynamic> location = msgBox.toMap();
    location.forEach((key, value) {
      if (UserInfo.userId==value.numberTaxi) {
        msgTaxi.add({
          'key': key.toString(),
          'nameUser': value.nameUser,
          'numberTaxi': value.numberTaxi,
          'numberUser': value.numberUser,
          "latUser": value.latUser,
          "longUser": value.longUser
        });
      }
    });
    update();
    print(msgTaxi);
  }

  Future<void> makeRead(String key) async{
    isLoading= true.obs;
    update();
   await msgBox.delete(int.parse(key));
    getAllMsg();
    isLoading=false.obs;
    update();
  }

}
