import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:taxi_driving/controller/get_location.dart';
import 'package:taxi_driving/controller/sample_bind.dart';
import 'package:taxi_driving/core/constant.dart';
import 'package:taxi_driving/view/custom/home_custom_screen.dart';
import 'package:taxi_driving/view/first_screen.dart';
import 'package:taxi_driving/view/taxi_office/home_taxi_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final controller = Get.put(GetLocationController());
  bool first = true;

  @override
  void initState() {
    super.initState();
    controller.getServiceEnabledAndPermissionStatus();
  }


  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;

    return GetBuilder(
      builder: (GetLocationController controller) {


          if (first) {
       Timer(const Duration(seconds: 3), () {
          if (UserInfo.isLogin!) {
            if (UserInfo.isTaxi!) {
              setState(() {
                first = false;
              });
              WidgetsBinding.instance!.addPostFrameCallback((_) =>
                  Get.to(() => const HomeTaxiScreen(), binding: SampleBind()));
              // WidgetsBinding.instance!.addPostFrameCallback((_) =>
              //     Navigator.of(context).pushReplacement(
              //         MaterialPageRoute(
              //           builder: (context) => const HomeTaxiScreen(),
              //         ) ));
              // WidgetsBinding.instance!.addPostFrameCallback(
              //         (_) => Navigator.of(context).pushReplacement(
              //       MaterialPageRoute(
              //         builder: (context) => MainScreen(),
              //       ),
              //     ));
            } else {
              setState(() {
                first = false;
              });
              WidgetsBinding.instance!.addPostFrameCallback((_) =>
                  Get.to(() => const HomeCustom(), binding: SampleBind()));
              // WidgetsBinding.instance!.addPostFrameCallback((_) =>
              //     Navigator.of(context).pushReplacement(
              //         MaterialPageRoute(
              //           builder: (context) => const HomeCustom(),
              //         ) ));

              // WidgetsBinding.instance!.addPostFrameCallback(
              //         (_) => Navigator.of(context).pushReplacement(
              //       MaterialPageRoute(
              //         builder: (context) => MainScreen(),
              //       ),
              //     ));
            }
          } else {
            setState(() {
              first = false;
            });
            WidgetsBinding.instance!.addPostFrameCallback((_) =>
                Get.to(() => const FirstScreen(), binding: SampleBind()));
            // WidgetsBinding.instance!.addPostFrameCallback((_) =>
            //     Navigator.of(context).pushReplacement(
            //         MaterialPageRoute(
            //           builder: (context) => const FirstScreen(),
            //         ) ));
            // WidgetsBinding.instance!
            //     .addPostFrameCallback((_) => Navigator.of(context).push(
            //           MaterialPageRoute(
            //             builder: (context) => const FirstScreen(),
            //           ),
            //         ));
          }
        });
        }
        return Scaffold(
          body: Stack(
            children: [
              Container(
                width: _size.width,
                height: _size.height,
                decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.orangeAccent,
                        Colors.orange,
                        Colors.deepOrangeAccent,
                        Colors.deepOrange,
                      ],
                      end: Alignment.bottomRight,
                      begin: Alignment.topLeft,
                    )),
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: SvgPicture.asset("assets/taxi-splash.svg"),
                ),
              )
            ],
          ),
        );

      }
    );
  }
}
