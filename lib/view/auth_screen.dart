import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taxi_driving/controller/auth_controller.dart';
import 'package:taxi_driving/controller/sample_bind.dart';
import 'package:taxi_driving/core/constant.dart';
import 'package:taxi_driving/core/widget/auth_field.dart';
import 'package:taxi_driving/core/widget/my_button.dart';
import 'package:taxi_driving/core/widget/my_text_button.dart';
import 'package:taxi_driving/core/widget/toast.dart';
import 'package:taxi_driving/view/custom/home_custom_screen.dart';
import 'package:taxi_driving/view/taxi_office/home_taxi_screen.dart';

class TaxiAuthScreen extends StatefulWidget {
  final bool isOffice;

  const TaxiAuthScreen({Key? key, required this.isOffice}) : super(key: key);

  @override
  _TaxiAuthScreenState createState() => _TaxiAuthScreenState();
}

class _TaxiAuthScreenState extends State<TaxiAuthScreen> {
  final _formKeyLogin = GlobalKey<FormState>();
  final _formKeySign = GlobalKey<FormState>();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _officeNameController = TextEditingController();
  final TextEditingController _numberController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final controller = Get.put(AuthController());

  @override
  void initState() {
    // controller.initAuth();
    controller.getAllNumbers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: const Icon(Icons.arrow_back_ios)),
        title: Text(
          widget.isOffice ? "التسجيل كمكتب تكاسي" : 'التسجيل كزبون',
          style: const TextStyle(color: Colors.white),
        ),
        centerTitle: true,
      ),
      extendBodyBehindAppBar: true,
      body: GetBuilder(
          builder: (AuthController controller) {
        if (controller.isValidateLogin.value) {
          toastMsg('الرجاء ادخال كلمة المرور الصحيحة');
          controller.clearIsValidateLogin();
        }

        if (UserInfo.isLogin!) {
          if (widget.isOffice) {
            WidgetsBinding.instance!.addPostFrameCallback((_) =>
                Get.to(() => const HomeTaxiScreen(), binding: SampleBind()));
          } else {
            WidgetsBinding.instance!.addPostFrameCallback((_) =>
            Get.to(() => const HomeCustom(), binding: SampleBind()));
          }
        }

        return SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                height: _size.height,
                width: _size.width,
                padding: const EdgeInsets.symmetric(horizontal: 15),
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/taxi-back.jpg'),
                        fit: BoxFit.fill)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AnimatedCrossFade(
                        firstChild: signUp(),
                        secondChild: login(),
                        crossFadeState: controller.showFirst.value
                            ? CrossFadeState.showFirst
                            : CrossFadeState.showSecond,
                        duration: const Duration(milliseconds: 500)),
                    SizedBox(height: _size.height * 0.04),
                    MyButton(
                      onPressed: () {
                        controller.validatorNumber(
                            number: _numberController.text);

                        if (controller.showFirst.value) {
                          if (!_formKeySign.currentState!.validate()) {
                            return;
                          }
                          FocusScope.of(context).unfocus();
                          controller.signUp(
                              name: _nameController.text,
                              password: _passwordController.text,
                              number: _numberController.text,
                              officeName: widget.isOffice
                                  ? _officeNameController.text
                                  : null);
                        } else {
                          if (!_formKeyLogin.currentState!.validate()) {
                            return;
                          }
                          FocusScope.of(context).unfocus();
                          if (controller.useNumber.isFalse) {
                            toastMsg('الرقم غير موجود');
                          }
                          else {
                            controller.validatorPasswordLogin(
                              password: _passwordController.text,
                              number: _numberController.text,
                              isOffice: widget.isOffice);
                          }
                        }
                      },
                      title: controller.showFirst.value
                          ? "تسجيل الحساب"
                          : "تسجيل الدخول",
                      width: _size.width,
                      height: 50.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const SizedBox(width: 10),
                        MyTextButton(
                            title: controller.showFirst.value
                                ? "هل لديك حساب؟"
                                : "ليس لدي حساب؟",
                            onPressed: () {
                              controller.changeShowFirst();
                            }),
                      ],
                    )
                  ],
                ),
              ),
              controller.isLoading.isTrue
                  ? SizedBox(
                      child: const Center(child: CircularProgressIndicator()),
                      height: _size.height,
                      width: _size.width,
                    )
                  : Container(),
            ],
          ),
        );
      }),
    );
  }

  Widget signUp() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
      elevation: 4,
      color: Colors.white,
      child: Form(
        key: _formKeySign,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              AuthField(
                  labelText: 'الاسم',
                  controller: _nameController,
                  iconPath: "assets/icons/user.svg",
                  hintText: "اسم",
                  validator: (val) {
                    if (_nameController.text.isEmpty) {
                      return 'لا تترك الحقل فارغ';
                    }
                    return null;
                  }),
              const SizedBox(
                height: 10,
              ),
              AuthField(
                  labelText: 'الرقم',
                  controller: _numberController,
                  iconPath: "assets/icons/phone.svg",
                  hintText: "+963",
                  keyboardType: TextInputType.number,
                  validator: (val) {
                    if (_numberController.text.isEmpty) {
                      return 'لا تترك الحقل فارغ';
                    }
                    if (controller.useNumber.value) {
                      return 'هذا الرقم موجود بالفعل';
                    }
                    if (int.tryParse(_numberController.text) == null) {
                      return 'الرجاء ادخال رقم صالح';
                    }
                    return null;
                  }),
              const SizedBox(height: 10),
              widget.isOffice
                  ? AuthField(
                      labelText: 'الاسم المكتب',
                      controller: _officeNameController,
                      iconPath: "assets/icons/user.svg",
                      hintText: "اسم المكتب",
                      validator: (val) {
                        if (_officeNameController.text.isEmpty) {
                          return 'لا تترك الحقل فارغ';
                        }
                        return null;
                      })
                  : const SizedBox(),
              widget.isOffice
                  ? const SizedBox(
                      height: 10,
                    )
                  : const SizedBox(),
              AuthField(
                  labelText: 'كلمة المرور',
                  controller: _passwordController,
                  iconPath: "assets/icons/padlock.svg",
                  hintText: "*****",
                  isPassword: true,
                  validator: (val) {
                    if (_passwordController.text.isEmpty) {
                      return 'لا تترك الحقل فارغ';
                    }
                    if (_passwordController.text.length < 6) {
                      return 'كلمة المرور قصيرة جداً';
                    }
                    return null;
                  }),
            ],
          ),
        ),
      ),
    );
  }

  Widget login() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
      elevation: 4,
      color: Colors.white,
      child: Form(
        key: _formKeyLogin,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              AuthField(
                  labelText: 'الرقم',
                  controller: _numberController,
                  iconPath: "assets/icons/phone.svg",
                  hintText: "+963",
                  keyboardType: TextInputType.number,
                  validator: (val) {
                    if (_numberController.text.isEmpty) {
                      return 'لا تترك الحقل فارغ';
                    }
                    if (int.tryParse(_numberController.text) == null) {
                      return 'الرجاء ادخال رقم صالح';
                    }
                    return null;
                  }),
              AuthField(
                  labelText: 'كلمة المرور',
                  controller: _passwordController,
                  iconPath: "assets/icons/padlock.svg",
                  hintText: "*****",
                  isPassword: true,
                  validator: (val) {
                    if (_passwordController.text.isEmpty) {
                      return 'لا تترك الحقل فارغ';
                    }
                    return null;
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
